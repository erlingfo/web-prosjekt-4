# Assignment 4

## User guide to run locally


### Required software

1. Node.js 
2. Yarn
3. The expo commandline tool (expo-cli)

## Installation and startup

#### Installation

1. Clone this project to your computer using git, or download the zip and unzip it
2. Using the command line interface, navigate to the my-project folder and run `yarn install`

During my own setup of this application, I got the error message elaborated upon in the link below. If you get this problem while installing the project, you could try the solution listed there.

https://stackoverflow.com/questions/58120990/how-to-resolve-the-error-on-react-native-start

### Starting the project
Using the command line interface, navigate to the my-project folder and run `yarn start`. This should open up the expo delvelopment tool in your browser. I assume you're already quite familiar on how to use expo with either an IOS/Android simulator, or on your physical mobile device, so I will not go into detail on how to use this.

**However** the api and database used in this project is located on an NTNU server, meaning all devices running the native app __have to be logged onto eduroam or use VPN__. If you are using VPN, you might have to switch the connection setting to tunnel and scan the QR code to open the project, as VPN tends to interfere with the LAN.

# Technology

## Frontend
For the frontend I have chosen to design most of the app solely using the standard react-native library with one exception: The header component, which I got from Paper io. The reason why I chose to use this instead of making my own header was because I was planning on using more components from Paper, but in the end I decided not to. The main reason for why I didn't use more components from third parties, was in order to brush up on my css skills a bit.

### Dependencies and plugins

#### Paper io
I use the `<Appbar.Header>` component, and style it using the default style of Appbar by wrapping the entire app component in PaperProvider.

### Async storage
The way I have implemented Async storage, it saves the previous search you ran. I use the standard react-native `AsyncStorage.getItem()` and `AsyncStorage.setItem()`. This functionality can easily be tested by searching for something and then reloading the app.

## Backend

### The API
In the previous project, my group implemented a REST api using Express.js. The reason for why I chose to reuse this was because I already had a lot of react code that interacted with it and was really easy to reuse. Here I truly see the advantage of react native and react being so closely related.

### Elastic search database
Elasticsearch is a document database based on apache Lucene. We decided to use this for our last project because it is a very flexible and fast database that has all the feautures we want. It should be noted that the search functionality that comes with elastic search is quite advanced, so you might be puzzeled at first as to whether or not it actually works properly. We have configured the API to run searches on the song title, album title and name of the artist, and a will not exclusively return results that completely match with the search string.

<img src="Images for README/Search.jpg" alt="Search Image" width="250">

As long as either the song title, album title or the name of the artist contains __one__ of the words in the search, the song should appear somewhere down the list of results. This means that if you order the list alphabetically, the song you searched for might no longer appear in the list.

<img src="Images for README/Sort_order.jpg" alt="Sort order picture" width="250">

To communicate with the database we used elastics own Node.js api. This has beed used to add data to the database as well as retriving data from it.
In `` we have our helping functions for the API, these functions are used to search, add user reviews to tracks and to get a single track by ID.

- More info on Node.js api https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/api-reference.html

## Testing

I have tested the app continuously during production, both on my physical iPhone X running iOS 12.3.1, and on a Google Pixel 3 emulator running the latest version of Android (Android 10). The app behaved the exact same way on both. The only exception is that the Paper io Appbar.Header appears in the middle of the screen on iOS, and to the left on Android.



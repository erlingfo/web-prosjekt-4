import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import {
    Provider as PaperProvider,
    Appbar,
    DefaultTheme
} from 'react-native-paper'

import Song from './components/Song/Song'
import SongList from './components/SongList/SongList'
import Header from './components/Header/Header'
import Searchbar from './components/Searchbar/Searchbar'

import SongFetcher from './components/SongFetcher/SongFetcher'
import SongService from './components/SongFetcher/SongService'

class App extends Component {

    state = {
        showMenu: false,
        songData: [],
    }

    songFetcher = new SongFetcher()

    // This function is passed as a prop to the Searchbar component, and will run the searchForSongAsync function in SongFetcher with the parameters from the state in Searchbar. It also puts the returning list of objects into the songData list in state
    executeSearch = async (searchParams) => {
        const results = await this.songFetcher.searchForSongAsync(searchParams)
        this.setState({
            songData: results.listOfSongs,
        })
    }

    // toggles the showMenu value in state which controls the filter/sort menu in Searchbar
    toggleMenu = () => {
        this.setState(prevState => ({
            showMenu: !prevState.showMenu
        }))
    }

    render() {
        let songData = this.state.songData

        const arrowUp = {uri: 'https://www.iconsdb.com/icons/preview/white/arrow-142-xxl.png'}
        const arrowDown = {uri: 'https://www.iconsdb.com/icons/preview/white/arrow-204-xxl.png'}

        const theme = {
          ...DefaultTheme,
          roundness: 2,
          colors: {
            ...DefaultTheme.colors,
            primary: '#222',
            accent: '#fff',
          },
        };

        return (
            <PaperProvider theme={theme}>
                <Header />
                <View style={styles.container}>
                    <Searchbar
                        executeSearch={this.executeSearch}
                        showMenu={this.state.showMenu}
                        offset={this.state.offset}
                    />
                    <TouchableOpacity style={styles.arrowButton} onPress={this.toggleMenu}>
                        {this.state.showMenu
                            ?
                            <Image source={arrowUp} style={styles.arrowImage} />
                            :
                            <Image source={arrowDown} style={styles.arrowImage} />
                        }
                    </TouchableOpacity>
                    <View style={styles.contentView}>
                            {songData.length > 0
                                ?
                                <SongList listOfSongs={songData} />
                                :
                                <Text style={styles.text}>No results</Text>
                            }
                    </View>
                </View>
            </PaperProvider>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#333',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 0,
        height: 'auto',
        overflow: 'hidden'
    },
    text: {
        fontSize: 25,
        color: 'white',
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center'
    },
    contentView: {
        flex: 1,
        backgroundColor: '#333',
        alignItems: 'center',
        height: '100%'
    },
    arrowButton: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#222',
        paddingBottom: 5
    },
    arrowImage: {
        flex: 1,
        maxWidth: 25,
        height: 20,
    }
});

export default App

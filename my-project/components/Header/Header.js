import * as React from 'react';
import { Appbar } from 'react-native-paper';

// This component renders the Header in the top of the app. It utilizes the header from the Appbar component in the react-native-paper library by Paper io

export default class Header extends React.Component {
  render() {
    return (
      <Appbar.Header>
        <Appbar.Content
          title="Spotifake"
          subtitle="by Erling O"
        />
      </Appbar.Header>
    );
  }
}

import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
} from 'react-native'

// This component renders the buttons used in for filtering and sorting the results

class FilterButton extends Component {

    // this.props.active is the the state of the prop this button is watching (sortOrder/filterBy).
    // If it is null, eg. sortOrder === null, that means the button is inactive.
    state = {
        active: !(this.props.active === null)
    }

    // toggle is a functiton passed down from the Searchbar component which will trigger whatever purpose the button has on a higher level. Either toggle the filter or what to sort on
    toggle = this.props.toggle

    handlePress = () => {
        this.setState(prevState => ({
            active: !prevState.active
        }))
        this.toggle()
    }

    render() {
        const active = this.state.active
        const text = this.props.text

        // This conditinal rendering is used to decide what styling the button should have. It has different styling for active (turned on) or inactive
        return(
            <View style={styles.container}>
                {active
                    ?
                    <TouchableHighlight onPress={this.handlePress} style={styles.active}>
                        <Text style={styles.activeText}>{text}</Text>
                    </TouchableHighlight>
                    :
                    <TouchableHighlight onPress={this.handlePress} style={styles.inActive}>
                        <Text style={styles.inActiveText}>{text}</Text>
                    </TouchableHighlight>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: 135
    },
    active: {
        backgroundColor: 'pink',
        padding: 20,
        borderRadius: 20,
    },
    activeText: {
        fontSize: 20,
        color: 'black',
        textAlign: 'center'
    },
    inActive: {
        backgroundColor: '#333',
        padding: 20,
        borderRadius: 20,
    },
    inActiveText: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center'
    }
})

export default FilterButton

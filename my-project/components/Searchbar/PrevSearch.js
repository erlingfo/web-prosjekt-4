import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native'

// This component renders the previous search stored in the Async storage.

class PrevSearch extends Component {

    // runs the handleSearchFromPrevSearch method in Searchbar.
    handlePress = () => {
        this.props.executeSearch()
    }

    render() {
        return(
            <View style={styles.container}>
                <Text style={styles.text}>Previous Search</Text>
                <TouchableOpacity style={{width: '100%'}} onPress={this.handlePress}>
                    <Text style={styles.text}>{this.props.prevSearch}</Text>
                </TouchableOpacity>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        width: '100%',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        height: 50
    },
    text: {
        color: 'white',
        fontSize: 23,
        textAlign: 'center'
    }

})

export default PrevSearch

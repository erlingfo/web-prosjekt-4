import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage
} from 'react-native'

import SongFetcher from '../SongFetcher/SongFetcher'
import FilterButton from './FilterButton'
import PrevSearch from './PrevSearch'

// This component handles the search parameters, and AsyncStorage. It renders all components related to search. This includes the dropdown menu for selection as well.

class Searchbar extends Component {

    // State contains all of the parameters to be used for search by the SongFetcher component.
    // Additionally, It contains AsyncStorageData; a parameter that stores the data retrieved from the Async storage of the device.
    state = {
            searchString: null,
            limit: '40',
            offset: null,
            filterBy: 'duration_ms',
            greaterThan: null,
            sortBy: 'name.keyword',
            sortOrder: null,

            AsyncStorageData: null
    }

    // executeSearch is a function passed down as a prop by the App component so that the search can be executed from a higher level in the system to be more easily managed.
    executeSearch = this.props.executeSearch
    songFetcher = new SongFetcher()

    // initially searches with an empty searchString. The setTimeout is there to ensure that this happenes after the initial render of app, so that this will appear imediately when one enters the app.
    componentDidMount = () => {
        this._retrieveData()
        this.handleSearch()
        // setTimeout(this.handleSearch, 0)
    }

    // executes the search by running the executeSearch method from App. It passes inn the current state object as a parameter.
    // It also makes sure to only update the Async storage if the search contains a searchString.
    handleSearch = async () => {;
        this.state.searchString && this._storeData()
        this.executeSearch(this.state)
        this._retrieveData()
    }

    // this method is passed down to the PrevSearch component to run on press
    handleSearchFromPrevSearch = async () => {
        this.setState({searchString: this.state.AsyncStorageData})
        setTimeout(this.handleSearch, 0)
    }

    // Toggles the sortOrder state between ascending (alphabetical) and null which will cause Elastic search to order by relevance
    handleSortChange = async () => {
            this.setState((prevState) =>(
                prevState.sortOrder === null ? {sortOrder: 'asc'} : {sortOrder: null}
            ))
            setTimeout(this.handleSearch, 0)
    }

    // Since the filterBy state is always set to duration_ms, all we need to change in order to toggle the filter is how many milli seconds the duration has to be longer than. 180000 is the equivalent of 3 min
    handleFilterChange = async () => {
            this.setState((prevState) =>(
                prevState.greaterThan == null ? {greaterThan: 180000} : {greaterThan: null}
            ))
            setTimeout(this.handleSearch, 0)
    }

    // handles the process of storing data in the AsyncStorage
    _storeData = async () => {
        try {
            await AsyncStorage.setItem('prevSearch', JSON.stringify(this.state.searchString))
        } catch (e) {
            console.error(e.message);
        }
    }

    // handles the process of retrieving data from the AsyncStorage. It will also update the AsyncStorageData value in state when it is retrieved.
    // The value variable is on the format "result". The slice removes the ""
    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('prevSearch');
            if (value !== null) {
                const stringValue = value.slice(1,value.length-1)
                this.setState({AsyncStorageData: stringValue})
            }
            return false
        } catch (error) {
            console.error(error.message);
        }
    }

    render() {

        // based on what the sort order is set to now, it will update the text on the button
        const sortButtonText = (this.state.sortOrder === null ? 'Relevance' : 'A-Z' )

        // a boolean which decides whether or not to show the filter menu
        const showMenu = this.props.showMenu

        return(
            <View style={styles.container}>
                <View style={styles.searchBarContainer}>
                    <TextInput
                        style={styles.searchbar}
                        placeholder=' Search...'
                        onChangeText={searchString => this.setState({searchString})}
                        onSubmitEditing={this.handleSearch}
                    />
                </View>
                {showMenu &&
                    <View >
                        <View>
                            <PrevSearch
                                prevSearch={this.state.AsyncStorageData}
                                executeSearch={this.handleSearchFromPrevSearch}
                            />
                        </View>
                        <View style={styles.filterMenu}>
                            <Text style={{fontSize: 25, color: 'white'}}>Sort By:</Text>
                            <FilterButton text={sortButtonText} toggle={this.handleSortChange} active={this.state.sortOrder} />
                        </View>
                        <View style={styles.filterMenu}>
                            <Text style={{fontSize: 25, color: 'white'}}>Filter:</Text>
                            <FilterButton text='>3 min' toggle={this.handleFilterChange} active={this.state.greaterThan} />
                        </View>
                    </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#222',
        width: '100%',
        paddingBottom: 10,
        paddingTop: 5
    },
    searchBarContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'flex-start',
        backgroundColor: '#222',
        width: '100%',
    },
    searchbar: {
        height: 50,
        borderColor: '#9c9b9a',
        backgroundColor:'#e1e3e8',
        width: '85%',
        borderWidth: 2,
        borderRadius: 15,
        fontSize: 25
    },
    filterMenu: {
        flexDirection: 'row',
        width: '70%',
        marginTop: 20,
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})

export default Searchbar

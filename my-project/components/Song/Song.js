import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Linking
} from 'react-native';

// This is the Song component. It renders the Song cards

class Song extends Component {

    state = {
        image: {uri: this.props.imageURL},
        name: this.props.name,
        artist: this.props.artist,
        album: this.props.album,
        spotifyURL: this.props.spotifyURL,
        duration: this.props.duration,
        collapsed: true
    }


    // changes the collapsed value. When false the card will display it's detailed view
    handlePress = () => {
        this.setState(prevState => (
            { collapsed: !prevState.collapsed }
        ))
    }

    render() {
        let image = this.state.image
        let name = this.state.name
        let artist = this.state.artist
        let album = this.state.album
        let duration = this.state.duration
        let collapsed = this.state.collapsed

        // This link will open the song in spotify when pressed using the react-native Linking component. It is what makes the Spotify button work
        const spotifyURL = this.state.spotifyURL
        const spotifyIcon = { uri: 'https://static.spin.com/files/2018/05/Spotify-Logo-1526659588-640x469.png'}

        // Conditionally renders the detailed view of each card individually.
        // This is done by using the {statement ? (return if true) : (return if false)} structure
        // In this case it's {collapsed ? normal card : detailed view}
        return(
                <View style={styles.container}>
                    {collapsed ?
                        <TouchableOpacity style={styles.songContainerCollapsed} onPress={this.handlePress}>
                                <Image source={image} style={styles.image}/>
                                <Text numberOfLines={2} style={styles.songName}>{name}</Text>
                        </TouchableOpacity>
                    :
                        <TouchableOpacity style={styles.songContainerFull} onPress={this.handlePress}>
                            <View style={styles.imagecontainerFull}>
                                <Image source={image} style={styles.imageFull}/>
                            </View>
                            <View style={styles.headerBox}>
                                <Text style={styles.songNameFull}>{name}</Text>
                                <Text style={styles.artist}>{artist}</Text>
                                <Text style={styles.album}>{album}</Text>
                                <Text style={styles.duration}>{duration} min</Text>
                            </View>
                            <TouchableOpacity
                                    style={styles.button}
                                    onPress={() => Linking.openURL(spotifyURL)}
                                >
                                    <Image style={styles.spotifyIcon} source={spotifyIcon} />
                                    <Text style={styles.buttonText}>Spotify</Text>
                            </TouchableOpacity>

                        </TouchableOpacity>
                    }
                </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#333',
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 10,
        justifyContent: 'space-around',
    },
    songContainerCollapsed: {
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#393b40',
        width: '95%',
        borderRadius: 40,
        height: 90,
    },
    songContainerFull: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#222',
        width: '95%',
        borderRadius: 40,
    },
    image: {
        flex: 1,
        maxWidth: 80,
        height: 80,
        borderRadius: 40,
        marginLeft: 5,
    },
    imageFull: {
        flex: 1,
        maxWidth: 200,
        height: 200,
        borderRadius: 40,
        marginBottom: 10,
        marginTop: 10
    },
    spotifyIcon: {
        flex: 1,
        maxWidth: 30,
        height: 30,
        borderRadius: 10,
        marginLeft: 2,
    },
    imagecontainerFull: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    songName: {
        fontSize: 22,
        color: 'white',
        paddingLeft: 20,
        maxWidth: 230
    },
    songNameFull: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        paddingBottom: 0
    },
    artist: {
        fontSize: 23,
        color: '#84888a',
        textAlign: 'center',
        paddingBottom: 5
    },
    album: {
        fontSize: 20,
        color: '#5c5e5e',
        textAlign: 'center',
        paddingBottom: 10
    },
    duration: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center'
    },
    headerBox: {
        flexDirection: 'column',
        alignItems: 'center',
        marginBottom: 20,
        maxWidth: 300
    },
    imageBoxFull: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        flex: 1,
        backgroundColor: '#111',
        alignItems: 'center',
        flexDirection: 'row',
        maxWidth: 150,
        padding: 15,
        borderRadius: 20,
        fontSize: 20,
        marginTop: 0,
        marginBottom: 10
    },
    buttonText: {
        flex: 2,
        paddingLeft: 10,
        fontSize: 22,
        color: 'white'
    }
})

export default Song

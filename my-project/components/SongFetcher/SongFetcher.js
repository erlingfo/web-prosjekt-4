import SongService from "./SongService"

// This is the component that
class SongFetcher {
    songData = [];
    totalHits;
    status;
    initialQuery;

    constructor() {
        this.songService = new SongService();
    }

    clearSongData = () => {
        this.songData = [];
    };

    // This is the method used to structure the URLSearchParams.
    // It takes in an object with the search parmeters found in the state of the
    // Searchbar component.
    searchForSongAsync = async (paramList) => {

        // Creates a new object which will only include the parameters
        // that currently have a value different than null
        let urlParamsObject = {};

        if (!(paramList.searchString === null || paramList.searchString === "")) {
            urlParamsObject['searchString'] = paramList.searchString
        }
        if (paramList.filterBy !== null) {
            urlParamsObject['filterBy'] = paramList.filterBy
        }

        // Since we filter by the duration of the song, greaterThan specifies the threshold which the song should be longer than in milli seconds
        if (paramList.greaterThan !== null) {
            urlParamsObject['greaterThan'] = paramList.greaterThan
        }
        if (paramList.sortBy !== null) {
            urlParamsObject['sortBy'] = paramList.sortBy
        }
        if (paramList.sortOrder !== null) {
            urlParamsObject['sortOrder'] = paramList.sortOrder
        }
        // limit specifies the limit of how many songs the API should return. The API will return 20 by default if nothing else is specified.
        if (paramList.limit !== null) {
            urlParamsObject['limit'] = paramList.limit
        }
        // Offset specifies how many results should be skipped in the returning list of songs. Eg. if offset is 20, your search will exclude the 20 first songs in the returning list. This is useful for pagination.
        if (paramList.offset !== null) {
            urlParamsObject['offset'] = paramList.offset
        }

        // Uses the SongService component to execute the request to the API with the new request string
        const urlParams = new URLSearchParams(urlParamsObject);
        const data = await this.songService.get(urlParams)

        // Extracts the data we're interested in for each individual song in an object
        // and puts the new song objects into a the songData list from state
        .then((data) => {
            this.clearSongData();
            let i = 0;
            data.body.hits.hits.forEach((song) => {
                song = song._source;
                this.songData.push({
                    id: song.id,
                    url: song.external_urls.spotify,
                    imageURL: song.album.images[1].url,
                    name: song.name,
                    artist: song.artists[0].name,
                    album: song.album.name,
                    duration: Math.floor(song.duration_ms / 60000),
                });
                i++;
            });
            this.totalHits = data.body.hits.total.value
        })
        const returnValues = {
            listOfSongs: this.songData,
            totalHits: this.totalHits
        }

        // returns the list of Song objects together with the total number of search results
        return(
            returnValues
        )
    }
}
export default SongFetcher

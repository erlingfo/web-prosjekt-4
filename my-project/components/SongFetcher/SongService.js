const webApiUrl = "http://it2810-67.idi.ntnu.no:5000/api/tracks";

// This component contains the HTTP request method used to interact with
// the API on the server. In this project I have only used HTTP GET

class SongService {

    // urlParams is a list of the search parameters to be sent in a get request to the API
    get = async (urlParams) => {
        const options = {
            method: "GET",
        };
        const request = new Request(webApiUrl + "?" + urlParams, options);
        const response = await fetch(request);
        return response.json();
    };
}

export default SongService;

import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    ScrollView,
} from 'react-native';

import Song from '../Song/Song'

class SongList extends Component {

    render() {

        // recieves the list of song object returned after a search as a prop from App
        let listOfSongs = this.props.listOfSongs

        // Maps the objects in the list into Song components and returns a ScrollView
        return(
            <ScrollView
                style={styles.container}
                keyboardDismissMode='on-drag'
                >
                {listOfSongs.map(song => (
                    <Song
                        key={song.id}
                        name={song.name}
                        artist={song.artist}
                        album={song.album}
                        duration={song.duration}
                        imageURL={song.imageURL}
                        spotifyURL={song.url}
                    />
                ))}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        height: 'auto',
    }
})

export default SongList
